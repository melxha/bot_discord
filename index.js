// cseid = "7bc2470b4d208b391"
//api = "AIzaSyBTWVk0G7M5i_LdF92LzISvwUrx6hWGcHo"

const Discord = require('discord.js')
const client = new Discord.Client()

const { prefix, token } = require('./config.json')

const funcao = require('./comando')
const bemVindo = require('./bemVindo')
const musica = require('./musica')
const request = require('./request')
const jogos = require('./jogos')

client.once('ready', () => {
    console.log('READY!')
    client.user.setActivity("Lutando contra a ditadura II")
})

client.on('guildMemberAdd', async membroNovo => {
    bemVindo.mensagem(membroNovo)
})

client.on('message', mensagem => {

    //criação dos comandos
    const comandosMsg = {
        ss: '@everyone, esse server é só seriedade e saliência!',
        genero: 'Foi confirmado pelos coringas que eu sou Trans-bi-skolio-solo-poli-demi-fluido-gray-sapio-sexual. É sobre isto!',
        ninja:"Nunca mexa com o poderosíssimo ninja! Estou ***F U M I N A N T E***",
        ideologia: "Em processo...",
        feromonio: "Bouas pessoal! Um genshin co pae?",
        ping: "Pong!",
        hoje: "Um belo dia!",
        beep: "Boop!",
        steve: "O homem ditador!!!",
        melchert: "O melxha",
        gordao: "A baleia!",
        mika: "O baiano!",
        rosa: "O neutro!",
        batista: 'A baqueta!',
        preto: "Formigona!",
        americans: "You are on my sight!",
        furry: "<@544551217028792329>",
        vegeta: 'https://tenor.com/view/dragonballz-gif-4896831',
        lgn: request.login,
        giga: request.flag,
        cep: request.cep,
        bandeiras: jogos.bandeiras,
        img: funcao.imgSearch,
        pinto: funcao.cabeca,
        tchau: funcao.tchau,
        cdl: funcao.cdl,
        clear: funcao.clear,
        coringas: funcao.coringas,
        server: funcao.server,
        user: funcao.user,
        vote: funcao.vote,
        facil: musica.facil,
        play: musica.play,
        stop: musica.stop,
        skip: musica.skip,
        pause: musica.pause,
        resume: musica.resume,
        desc: funcao.desconectar,
        queue: musica.queue,
        back: musica.back,
        help: funcao.help,
    }
    //tratamento dos comandos
    
    const comandosChaves = Object.keys(comandosMsg)
    const comandosValores = Object.values(comandosMsg)

    let conteudo = mensagem.content
    comandosChaves.forEach((cmd, index) => {
        let comando = `${prefix}${cmd}`
        if (conteudo.startsWith(comando) || conteudo == comando) {
            if (typeof comandosValores[index] == 'string') {
                if (comandosValores[index] != "") mensagem.channel.send(comandosValores[index])
                else mensagem.channel.send('Falha')
            } else if (typeof comandosValores[index] == 'function'){
                if (comandosValores[index] != "undefined") {
                    comandosValores[index](mensagem, comandosChaves)
                }
                else mensagem.channel.send('Falha')
            }
        }
        
    })
})

client.login(token)