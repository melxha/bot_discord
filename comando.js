const Discord = require('discord.js')
const Scraper = require('images-scraper')

const google = new Scraper({
    puppeteer: {
        headless: true
    }
})

function numeroRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min 
}

function role(mensagem) {
    let cargosUser = mensagem.member.roles.cache
        .sort((a, b) => b.position - a.position)
        .map((r) => r)
        .join(", ")
    if (cargosUser.length > 1024) rolemap = "Muito cargo bicho."
    if (!cargosUser) cargosUser = "Sem cargos!"
    return cargosUser
}

module.exports = {
    imgSearch: async function(mensagem) {
        const args = mensagem.content.split(" ")
        const search = args.slice(1).join(" ")
        if(!args[1]) return mensagem.channel.send("Comando inválido, digite o que você quer pesquisar.")
        

        const index = numeroRandom(0, 8)
        const image_results = await google.scrape(search, 10)
        if (image_results == undefined) return mensagem.channel.send("Não encontrei nada!")
        mensagem.channel.send(image_results[index].url)
        mensagem.channel.send(image_results[index+1].url)
    },
    user: function(mensagem) {
        const user = mensagem.mentions.users.first() || mensagem.member.user
        const member = mensagem.guild.members.cache.get(user.id)
        const roles = member.roles.cache
        const cargos = []
        roles.map((arr) => {
            cargos.push(`<@&${arr.id}>`)
        })
        const index = cargos.length - 1
        cargos.splice(index)
        const avatar = user.displayAvatarURL({ dynamic: true, size: 1024 })
        const data = new Date(member.joinedTimestamp).toLocaleDateString()
        const embed = new Discord.MessageEmbed()
          .setTitle(`Perfil de ${user.username}: `)
          .addField("Cargos: ", cargos)
          .addField("Entou em: ", data)
          .setImage(avatar)
          .setColor("RANDOM");
      
        return mensagem.channel.send(embed)
    },
    cabeca: function(mensagem) {
        mensagem.channel.send("Cabeça!!")
        setTimeout(() => {
            mensagem.channel.send('Nada!')
        }, 10000)
    },
    tchau: function(mensagem) {
        var d = new Date()
        switch (d.getHours()) {
            case 0:
                mensagem.guild.members.cache.filter((member) => {
                  member.voice.kick()
                });
                mensagem.channel.send("Boa noite a todos!");
                break;
            
            case 1:
                mensagem.guild.members.cache.filter((member) => {
                  member.voice.kick()
                });
                mensagem.channel.send("Boa noite a todos!");
                break;

            case 2:
                mensagem.guild.members.cache.filter((member) => {
                  member.voice.kick()
                });
                mensagem.channel.send("Boa noite a todos!");
                break;

            case 3:
                mensagem.guild.members.cache.filter((member) => {
                  member.voice.kick()
                });
                mensagem.channel.send("Boa noite a todos!");
                break;

            case 4:
                mensagem.guild.members.cache.filter((member) => {
                  member.voice.kick()
                });
                mensagem.channel.send("Boa noite a todos!");
                break;

            case 5:
                mensagem.guild.members.cache.filter((member) => {
                  member.voice.kick()
                });
                mensagem.channel.send("Boa noite a todos!");
                break;
        
            default:
                mensagem.channel.send("Ainda não está na hora de dormir!")
                break;
        }
        
    },
    coringas: function(mensagem) {
        let resultado = []
        mensagem.guild.members.cache.filter((member) => {
            member._roles.forEach((cargos) => {
                if (cargos == "803693179156168705") {
                    resultado.push(`<@${member.user.id}>`)
                }
            })
        })
        if (resultado.length > 0) {
            return mensagem.channel.send(`Os coringas que estão online: \n${resultado.join(", ")}!`)
        } else {
             return mensagem.channel.send(`Não há coringas onlines!`)
        }
    },
    desconectar: function(mensagem) {
        if (mensagem.member.hasPermission("KICK_MEMBERS")) {
            if (mensagem.member.id == '407227008989265941') return mensagem.channel.send("Ditadura comigo aqui não!")
            if (!mensagem.mentions.users.size) {
                return mensagem.reply("Use a tag de alguém para desconectar.")
            } else {
                mensagem.guild.members.cache.filter((member) => {
                    if (member.id == mensagem.mentions.users.first().id) {
                        member.voice.kick()
                    }
                })
                const taggedUser = mensagem.mentions.users.first()
                return mensagem.channel.send(`Você desconectou: ${taggedUser}`)
            }
        } else {
            return mensagem.reply("Você não tem permissão")
        }
    },
    server: function(mensagem) {
        function roles() {
            let cargos = mensagem.guild.roles.cache
              .sort((a, b) => b.position - a.position)
              .map((r) => r)
              .join(", ")
            if (cargos.length > 1024) cargos = "To many roles to display"
            if (!cargos) cargos = "No roles"
            return cargos
        }

        const cargos = roles()
        const server_info = {
            nome: `Nome do Servidor: ${mensagem.guild.name}`,
            membrosQtde: `Total de membros: ${mensagem.guild.memberCount}`,
            cargos: `Cargos: ${cargos}`,
            dono: `Dono: ${mensagem.guild.owner}`,
        }
        let avatar
        if (mensagem.guild.id == '407228373018869760') avatar = 'https://cdn.discordapp.com/icons/407228373018869760/ca382f42328a31bf416782c31c2cbdda.png?size=1024'
        else if (mensagem.guild.id == '755461706083336203') avatar = 'https://cdn.discordapp.com/icons/755461706083336203/7dfa237bf28c1c380e551fad3814a434.png?size=1024'
        else avatar = "./"
        const data = new Date(mensagem.guild.createdTimestamp).toLocaleDateString()

        const embed = new Discord.MessageEmbed()
          .setTitle(`Servidor ${mensagem.guild.name}: `)
          .addField("Cargos: ", cargos)
          .addField("Criado em: ", data)
          .addField("Dono: ", mensagem.guild.owner)
          .setImage(avatar)
          .setColor("RANDOM")

        return mensagem.channel.send(embed)
    },
    cdl: function(mensagem) {
        const array = [
            'Para, que eso porra!',
            'Filha da fruta!',
            'Pega com jeitinho.',
            'Cafunga a careca vai!',
            'Então segura o brigadeiro'
        ]
        const index = numeroRandom(0, array.length-1)
        return mensagem.channel.send(array[index])
    },
    clear: function(mensagem) {
        const args = mensagem.content.split(" ")
        if (!args[1]) return mensagem.channel.send("Digite a quantidade linhas a ser apagadas!")
        let qtde = parseInt(args[1])
        if (qtde > 51) qtde = 51
        let msg
        if (qtde == 1) msg = 'mensagem'
        else msg = 'mensagens'
        mensagem.channel.bulkDelete(qtde).then(() => {
            mensagem.channel.send(`Foi deletado ***${qtde}***  ${msg}!`)
        })
    },
    vote: async function(mensagem) {
        if(mensagem.channel.id != '822195597049593896' && mensagem.channel.id != '818859371085430814') return mensagem.channel.send("Você não está na Corte Coringonal!")
        
        const tipos = [
            'kick',
            'preferencia',
            'cargo'
        ]

        const cargos = role(mensagem)
        
        let categoriaVotacao
        mensagem.channel.send('A votação será sobre o que? \nEscola entre: **' + tipos.join(', ') + '**!')
        try {
            categoriaVotacao = await mensagem.channel.awaitMessages(msg => tipos.includes(msg.content) , {
                max: 1,
                time: 15000,
                errors: ['time']
            })
        } catch {
            return mensagem.channel.send("Escolha inválida champs.")
        }
        const categoria = categoriaVotacao.first().content
        mensagem.channel.send("Você escolheu a categoria: **" + categoria + "**!")

        if (categoria == 'kick') {
            mensagem.channel.send("Quantas pessoas vão participar? O mínimo para participar desta votação é 3!")
            let qtde

            try {
                await mensagem.channel.awaitMessages(msg => msg.content >= 3 , {
                    max: 1,
                    time: 15000,
                    errors: ['time']
                }).then(msg => qtde = msg.first().content)
            } catch {
                return mensagem.channel.send("Escolha inválida champs.")
            }

            mensagem.channel.send("Quem é essa pessoa?")
            let mencao
            try {
                await mensagem.channel.awaitMessages(msg => msg.mentions.users.first(), {
                    max: 1,
                    time: 15000,
                    errors: ['time']
                }).then(msg => {
                    mencao = msg.first().mentions.users.first().id
                })
            } catch {
                return mensagem.channel.send("Escolha inválida champs.")
            }
            mensagem.channel.send('Agora votem! Digite: **1** para o **sim** e **2** para o **não**')
            let sim = 0
            let nao = 0
            try {
                await mensagem.channel.awaitMessages(msg => msg.content == 1 || msg.content == 2, {
                    max: qtde,
                    time: 60000,
                    errors: ['time']
                }).then(msg => {

                    const valores = Object.values(msg.first(qtde))
                    let autorCond = 0
                    let autores = []
                    valores.forEach(element => {
                        if (autores.includes(element.author.id)) autorCond = 1
                        else autores.push(element.author.id)
                        element.content == 1 ? sim++ : nao++
                    })

                    if (autorCond == 1) return mensagem.channel.send("Muito bom você quer votar 2x, assim você vai longe! \nComeça de novo a votação.")
                    else mensagem.channel.send(`Houve ${sim} votos para o sim, e ${nao} votos para o não!`)
                    if (sim > nao) {
                        mensagem.guild.members.cache.filter(member => {
                            if (member.id == mencao) {
                                mensagem.channel.send(`Grande abraço <@${mencao}>!`)
                                return member.voice.kick()
                            }
                        })
                    }
                    else return mensagem.channel.send("O povo não quer, e quando o povo não quer eu não faço!")
                })
            } catch {
                return mensagem.channel.send("Vocês não parecem ter certeza sobre essa votação.")
            }
        } else if (categoria == 'preferencia') {

        } else if (categoria == 'cargo') {

        }
        
        return 
    },
    python: (mensagem) => {
        
    },
    help: function(mensagem, comandosMsg) {
        const embed = new Discord.MessageEmbed()
          .setTitle(`Os comandos são:`)
          .addField("! ", comandosMsg.join(", "))
          .setColor("RANDOM");
      return mensagem.channel.send(embed)
    },
}