const Discord = require("discord.js")
const client = new Discord.Client()
const ytdl = require("ytdl-core")
const ytpl = require('ytpl')
const ytSearch = require("yt-search")

let queue = []
let back = []
let queue2 = []

const players = [{},{},{}]

const videoFinder = async (query, mensagem, condicaoVideo) => {
    const videoResult = await ytSearch(query)
    if (condicaoVideo != 1) {
        const musicas = []
        for (let c = 0; c < 5; c++) {
            musicas.push("***" + parseInt(c+1) + ".***   " + videoResult.videos[c].title)
        }
     
        const embed = new Discord.MessageEmbed()
          .setTitle(`Músicas: `)
          .addField("Escolha entre: ", musicas.join(".\n\n"))
          .setColor("RANDOM")
        mensagem.channel.send(embed)
        try {
            var res = await mensagem.channel.awaitMessages(msg => msg.content > 0 && msg.content < 6, {
                max: 1,
                time: 10001,
                errors: ['time']
            })
        } catch {
            mensagem.channel.send("Escolha inválida champs.")
        }
        
        const escolha = parseInt(res.first().content)
        return (videoResult.videos.length > 1) ? videoResult.videos[escolha-1] : null
    }
    
}


async function playSong(mensagem, fila, acao) {
    const canalVoz = mensagem.member.voice.channel
    const connection = await canalVoz.join()
    copia = fila

    if (fila[0].tipo == 'achado') {
        const embed = new Discord.MessageEmbed()
          .setTitle(`Tocando: ${fila[0].titulo} `)
          .addField(`Duração: `, fila[0].tempo)
          .addField(`Por: `, fila[0].autor)
          .setImage(fila[0].thumbnail)
          .setColor("RANDOM")
        
        const stream = ytdl(fila[0].url, { filter: "audioonly" })
        const dispatcher = connection.play(stream, { seek: 0, volume: 1 })
        if (acao == 'play') {
            dispatcher
            .on("start", () => {
                mensagem.channel.send(embed)
            })
            dispatcher
            .on('finish', () => {
                back = []
                back.push(fila[0])
                mensagem.channel.send(`<@${fila[0].membro}>, pediu para tocar essa maçã!`)
                if (!canalVoz) canalVoz.join()
                queue = queue.slice(1)
                if (queue.length < 1) {
                    setTimeout(() => {
                        if (queue.length < 1) canalVoz.leave()
                    }, 60000)
                    
                } else {
                    playSong(mensagem, queue, 'play')
                }
            })
        }
        

        else if (acao == 'pause') {
            dispatcher.pause()
            const embed = new Discord.MessageEmbed()
              .setTitle(`Pausando: ${fila[0].titulo} `)
              .setColor("RANDOM")
            mensagem.channel.send(embed)
        }
        else if (acao == 'resume') {
            dispatcher.resume()
            const embed = new Discord.MessageEmbed()
              .setTitle(`Voltando: ${fila[0].titulo} `)
              .setColor("RANDOM")
            mensagem.channel.send(embed)
            dispatcher
            .on('finish', () => {
                back = []
                back.push(fila[0])
                mensagem.channel.send(`<@${fila[0].membro}>, pediu para tocar essa maçã!`)
                if (!canalVoz) canalVoz.join()
                queue = queue.slice(1)
                if (queue.length < 1) {
                    
                    setTimeout(() => {
                        
                        if (queue.length < 1) canalVoz.leave()
                    }, 60000)
                    
                } else {
                    playSong(mensagem, queue, 'play')
                }
            })
        }
        return 
    } else if (fila[0].tipo == 'playlist') {
        const embed = new Discord.MessageEmbed()
          .setTitle(`Tocando: ${fila[0].titulo} `)
          .addField(`Duração: `, fila[0].timestamp)
          .addField(`Por: `, fila[0].autor)
          .setImage(fila[0].thumbnail)
          .setColor("RANDOM")
        
        const stream = ytdl(fila[0].url, { filter: "audioonly" })
        const dispatcher = connection.play(stream, { seek: 0, volume: 1 })

        if (acao == 'play') {
            dispatcher 
            .on("start", () => {
                mensagem.channel.send(embed)
            })
            dispatcher
            .on('finish', () => {
                back = []
                back.push(fila[0])
                mensagem.channel.send(`<@${fila[0].membro}>, pediu para tocar essa maçã!`);
                if (!canalVoz) canalVoz.join()
                queue = queue.slice(1)
                if (queue.length < 1) {
                    
                    setTimeout(() => {
                        
                        if (queue.length < 1) canalVoz.leave()
                    }, 60000)
                    
                } else {
                    playSong(mensagem, queue, 'play')
                }
            })
        }
        else if (acao == 'pause') {
            dispatcher.pause()
            const embed = new Discord.MessageEmbed()
              .setTitle(`Pausando: ${fila[0].title} `)
              .setColor("RANDOM")
            mensagem.channel.send(embed)
        }
        else if (acao == 'resume') {
            dispatcher.resume()
            const embed = new Discord.MessageEmbed()
              .setTitle(`Voltando: ${fila[0].title} `)
              .setColor("RANDOM")
            mensagem.channel.send(embed)
        }

        return 
    } else {
        const embed = new Discord.MessageEmbed()
          .setTitle(`Tocando: ${fila[0].title} `)
          .setColor("RANDOM")
        
        const stream = ytdl(fila[0].title, { filter: "audioonly" })
        const dispatcher = connection.play(stream, { seek: 0, volume: 1 })

        if (acao == 'play') {
            dispatcher
            .on("start", () => {
                mensagem.channel.send(embed)
            })
            dispatcher
            .on('finish', () => {
                back = []
                back.push(fila[0])
                mensagem.channel.send(`<@${fila[0].membro}>, pediu para tocar essa maçã!`);
                if (!canalVoz) canalVoz.join()
                queue = queue.slice(1)
                if (queue.length < 1) {
                    
                    setTimeout(() => {
                        
                        if (queue.length < 1) canalVoz.leave()
                    }, 60000)
                    
                } else {
                    playSong(mensagem, queue, 'play')
                }
            })
        }
        else if (acao == 'pause') {
            dispatcher.pause()
            const embed = new Discord.MessageEmbed()
              .setTitle(`Pausando: ${fila[0].title} `)
              .setColor("RANDOM")
            mensagem.channel.send(embed)
        }
        else if (acao == 'resume') {
            dispatcher.resume()
            const embed = new Discord.MessageEmbed()
              .setTitle(`Voltando: ${fila[0].title} `)
              .setColor("RANDOM")
            mensagem.channel.send(embed)
        }

        return
    }
    
}
async function playSong2(mensagem, fila) {
    const canalVoz = mensagem.member.voice.channel
    const connection = await canalVoz.join()

    const embed = new Discord.MessageEmbed()
      .setTitle(`Tocando: ${fila[0].titulo} `)
      .addField(`Duração: `, fila[0].timestamp)
      .addField(`Por: `, fila[0].autor)
      .setImage(fila[0].thumbnail)
      .setColor("RANDOM")
    
    const stream = ytdl(fila[0].url, { filter: "audioonly" })
    connection.play(stream, { seek: 0, volume: 1 })
    .on('finish', () => {
        if (!canalVoz) canalVoz.join()
        queue2 = queue2.slice(1)
        if (queue2.length < 1) {
            queue2 = []
            canalVoz.leave()
        } else {
            playSong2(mensagem, queue2)
        }
    })
    return mensagem.channel.send(embed)
    
}

module.exports = {
    play: async function tocar(mensagem) {
        //if (mensagem.author.id == "407227008989265941") return mensagem.channel.send("Vai morar no sul steve", {
        //  tts: true,
        //})
        if (mensagem.channel.type === "dm") return;
        const argumentos = mensagem.content.split(" ")
        const args = Object.values(argumentos).slice(1)
        let list = []
        let condicaoVideo = ''
        let playlistId = ''
        if (args[0].startsWith('https')) {
            list = args[0].split('=')
            if (list.length == 3) playlistId = list[2]
            else if (list.length == 2 && list[0].endsWith('list')) playlistId = list[1]
            condicaoVideo = 1
        }
        const canalVoz = mensagem.member.voice.channel

        if (!canalVoz) return mensagem.channel.send('Você precisa estar em um canal de voz!')
        if(args[0] == undefined) return mensagem.channel.send('Comando inválido')
        
        let video = ''
        let videoArr = []
        let condicao
        if (!args[0].startsWith('https') && playlistId == '') {
            condicao = 0
            video = await videoFinder(args.join(" "), mensagem, condicaoVideo)
        } else if (playlistId != '') {
            async function procuraPl(args, mensagem) {
                const playlist = await ytpl(args)
                playlist.items.forEach(async songs => {

                    videoArr.push({
                        tipo: 'playlist',
                        titulo: songs.title,
                        thumbnail: songs.bestThumbnail.url,
                        autor: songs.author.name,
                        url: songs.shortUrl,
                        tempo: songs.duration,
                        membro: mensagem.member.id
                    })
                }) 
            }
            await procuraPl(playlistId, mensagem)
            
            
        } else  {
            video = args[0]
            condicao = 1
        }

        if (mensagem.guild.id == '407228373018869760' || mensagem.guild.id == '375402391345823745') {

            if (video != "") {
                if (condicao == 0) {
                    queue.push({
                        tipo: 'achado',
                        titulo: video.title,
                        thumbnail: video.thumbnail,
                        timestamp: video.timestamp,
                        autor: video.author.name,
                        url: video.url,
                        tempo: video.seconds,
                        membro: mensagem.member.id
                    })
                
                    if (queue.length == 1) {
                        playSong(mensagem, queue, 'play')
                    } else if (queue.length > 1) {
                        const index = queue.length - 1;
                        const embed = new Discord.MessageEmbed()
                          .setTitle(`Música adicionada: ${queue[index].titulo} `)
                          .setColor("RANDOM");
                        return mensagem.channel.send(embed);
                    } 
                    
                } else {
                    queue.push({
                        tipo: 'url',
                        title: video,
                        membro: mensagem.member.id
                    })
                
                    if (queue.length == 1) {
                        playSong(mensagem, queue, 'play')
                    } else if (queue.length > 1) {
                        const index = queue.length - 1;
                        const embed = new Discord.MessageEmbed()
                          .setTitle(`Música adicionada: ${queue[index].url} `)
                          .setColor("RANDOM");
                        return mensagem.channel.send(embed);
                    } 
                }
                
            } else if (videoArr.length > 0){
                setTimeout(() => {
                    if (queue.length == 0) {
                        videoArr.forEach(element => {
                            queue.push(element)
                        })
                        playSong(mensagem, queue, 'play')
                    } else {
                        videoArr.forEach(element => {
                            queue.push(element)
                        })
                        const embed = new Discord.MessageEmbed()
                            .setTitle(`Playlist adicionada! `)
                            .addField('Use "!queue" para ver o que tem nela!')
                            .setColor("RANDOM");
                        return mensagem.channel.send(embed)
                    }
                }, 5000)
            } else {
                return mensagem.channel.send('Video não encontrado!')
            }
            
        } else {
            if (video) {

                mensagem.channel.send("Testing here")
                queue2.push({
                    titulo: video.title,
                    thumbnail: video.thumbnail,
                    timestamp: video.timestamp,
                    autor: video.author.name,
                    url: video.url,
                    tempo: video.seconds,
                })

                if (queue2.length == 1) {
                    playSong2(mensagem, queue2);
                } else if (queue2.length > 1) {
                    const index = queue2.length - 1;
                    const embed = new Discord.MessageEmbed()
                      .setTitle(`Música adicionada: ${queue2[index].titulo} `)
                      .addField(`Por: `, queue2[index].autor)
                      .setColor("RANDOM");
                    return mensagem.channel.send(embed);
                } else {
                    return mensagem.channel.send("Não há mais músicas na fila!");
                }
                

            } else {
                return mensagem.channel.send('Video não encontrado!')
            }
        }
        

    },
    stop: function parar(mensagem) {
        if (mensagem.guild.id == '407228373018869760' || mensagem.guild.id == '375402391345823745') {
            if (mensagem.channel.type === "dm") return
            if (!mensagem.member.voice.channel) return 'Você não está na sala do bot!'
            if (queue.length > 0) {
                const canalVoz = mensagem.member.voice.channel
                queue = []
                back = []
                canalVoz.leave()
                return mensagem.channel.send("P A R A D O!   Então tchau...   	(O -_- o)")
            } else {
                return mensagem.channel.send("Não estou tocando nada!")
            }
        } else {
            if (mensagem.channel.type === "dm") return
            if (!mensagem.member.voice.channel) return 'Você não está na sala do bot!'
            if (queue2.length > 0) {
                const canalVoz = mensagem.member.voice.channel
                queue2 = []
                canalVoz.leave()
                return mensagem.channel.send("P A R A D O!   Então tchau...   	(O -_- o)")
            } else {
                return mensagem.channel.send("Não estou tocando nada!")
            }
        }
    },
    skip: function pular(mensagem) {
        if (mensagem.guild.id == '407228373018869760' || mensagem.guild.id == '375402391345823745') {
            if (mensagem.channel.type === "dm") return
            const args = mensagem.content.split(' ')

            let qtde
            if (!args[1]) qtde = 1
            else qtde = parseInt(args[1])
            if (isNaN(qtde)) qtde = 1
            
            queue = queue.slice(qtde)
            if (queue.length > 0) {
                playSong(mensagem, queue, 'play')
                return mensagem.channel.send("S K I P A D O  " + qtde +" música(s)!   Bó de proxima então...   ┐ (¯ ヘ ¯) ┌")
            } else {
                const canalVoz = mensagem.member.voice.channel
                canalVoz.leave()
                return mensagem.channel.send("Não há mais músicas na fila!   ┐ (¯ ヘ ¯) ┌")
            }
        } else {
            if (mensagem.channel.type === "dm") return
            queue2 = queue2.slice(1)
            if (queue2.length > 0) {
                playSong(mensagem, queue2)
                return mensagem.channel.send("S K I P A D O!   Bó de proxima então...   ┐ (¯ ヘ ¯) ┌")
            } else {
                const canalVoz = mensagem.member.voice.channel
                canalVoz.leave()
                return mensagem.channel.send("Não há mais músicas na fila!   ┐ (¯ ヘ ¯) ┌")
            }
        }
        
    },queue: async function verFila(mensagem) {
        if (mensagem.guild.id == '407228373018869760' || mensagem.guild.id == '375402391345823745') {
            if (queue.length > 0) {
                let musica
                queue.length > 1 ? musica = 'músicas' : musica = 'música'
                let string = ''
                let max
                if (queue.length > 10) max = 10
                else max = queue.length
                for (let c = 0; c < max; c++) {
                    string += "***" + (c+1).toString() + ".***  " + queue[c].titulo + '\n'
                }
                const embed = new Discord.MessageEmbed()
                  .setTitle(`Lista de ${queue.length} ${musica} (max: 10 mostradas)! `)
                  .addField(`Fila: `, string)
                  .setColor("RANDOM")
                return mensagem.channel.send(embed)

            } else {
                return mensagem.channel.send("Não há mais músicas na fila")
            }
        } else {
            if (queue2.length > 0) {
                let string = ''
                queue2.forEach((element, index) => {
                    string += (index+1).toString() + ". " + element.titulo + '\n'
                    return string
                })
                const embed = new Discord.MessageEmbed()
                  .setTitle(`Lista de músicas! `)
                  .addField(`Músicas: `, string)
                  .setColor("RANDOM");
                return mensagem.channel.send(embed);

            } else {
                return mensagem.channel.send("Não há mais músicas na fila")
            }
        }
        
    },
    facil: async function (mensagem) {
        if (mensagem.channel.type === "dm") return
        const canalVoz = mensagem.member.voice.channel

        if (!canalVoz) return mensagem.channel.send('Você precisa estar em um canal de voz!')

        //const connection = await canalVoz.join()

        const videoFinderFacil = async query => {
            const videoResult = await ytSearch(query)
            return (videoResult.videos.length > 1) ? videoResult.videos[0] : null
        }

        const video = await videoFinderFacil("Jota quest facil audio")

        if (video) {

            queue.push({
                tipo: 'achado',
                titulo: video.title,
                thumbnail: video.thumbnail,
                timestamp: video.timestamp,
                autor: video.author.name,
                url: video.url,
                tempo: video.seconds,
                membro: mensagem.member.id
            })

            if (queue.length == 1) {
                playSong(mensagem, queue, 'play')
            } else if (queue.length > 1) {
                const index = queue.length - 1
                const embed = new Discord.MessageEmbed()
                  .setTitle(`Música adicionada: ${queue[index].titulo} `)
                  .addField(`Por: `, queue[index].autor)
                  .setColor("RANDOM")
                return mensagem.channel.send(embed)
            } else {
                return mensagem.channel.send("Não há mais músicas na fila!")
            }
        } else {
            return mensagem.channel.send('Video não encontrado!')
        }

    },
    back: function(mensagem) {
        queue.unshift(back)
        playSong(mensagem, back)
    },
    pause: function(mensagem) {
        queue.length > 0 ? playSong(mensagem, queue, 'pause') : mensagem.channel.send("Não tô fazendo nada meu chapa!")
    },
    resume: function(mensagem) {
        queue.length > 0 ? playSong(mensagem, queue, 'resume') : mensagem.channel.send("Não tô fazendo nada meu chapa!")
    }
}