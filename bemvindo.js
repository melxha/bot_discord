const Discord = require("discord.js");
const client = new Discord.Client();

function numeroRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}

module.exports = {
    mensagem: function msg(membro) {
        // add frases de bemvindo
        const mensagens = [
            'Seja muito bem-vindo ao império Coringonal',
            'Olá meu caro',
        ]
        
        let index = numeroRandom(0, mensagens.length - 1)

        const canalId = "819300894709252176"
        const conversaId = "723239079432683610"
        const conversa = membro.guild.channels.cache.get(conversaId).toString()

        const mensagem = `!user <@${membro.id}> entrou! ${mensagens[index]}, este é o canal de texto -> ${conversa}, Divirta-se`;
        const canal = membro.guild.channels.cache.get(canalId)
        canal.send(mensagem)
    }
}