const axios = require("axios")
const Discord = require("discord.js")

  module.exports = {
    login: function(mensagem) {
      const msg = mensagem.content.replace('!lgn ','').split(' ')
      const login = msg[0]
      const senha = msg[1]

      const data = {
        data: {
          login: login.replaceAll(' ', ''),
          senha: senha.replaceAll(' ', '')
        },
        banco: [
          {
            lgn: 'rosa',
            senha: 'neutro'
          }, 
          {
            lgn: 'mikaese',
            senha: 'baiano'
          }, 
          {
            lgn: 'gordo',
            senha: 'baleia'
          }, 
          {
            lgn: 'melchert',
            senha: '123'
          }
        ]
      }

      axios.post('http://127.0.0.1:5000/login', data, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => {
          console.log(res.data, res.status)
          if (res.data.success) {
            mensagem.channel.send(res.data.message)
          } else {
            mensagem.channel.send(res.data.message)
          }
        })
        .catch(e => console.log(e))
    },
    cep: function(mensagem) {
      let arg = mensagem.content.replace('!cep ','')
      const msg = arg.replace(' ', '')
      if (msg.length > 0) {
        console.log(msg)
        const data = {
          cep: msg
        }
        axios.post('http://127.0.0.1:5000/cep', data, {
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(res => {
            res = res.data.webservicecep.retorno
            console.log(res)
            if (res.resultado_txt == 'sucesso. cep encontrado local') {
              const log = res.tipo_logradouro + ' ' + res.logradouro
              mensagem.channel.send("CEP encontrado!")
              const embed = new Discord.MessageEmbed()
                .setTitle(`CEP: ${res.cep} `)
                .addField(`UF: `, res.uf)
                .addField(`Cidade: `, res.cidade)
                .addField(`Bairro: `, res.bairro)
                .addField(`Logradouro: `, log)
                .addField(`Latitude: `, res.latitude)
                .addField(`Longitude: `, res.longitude)
              mensagem.channel.send(embed)
            } else {
              mensagem.channel.send('Não encontrei irmão mal ae')
            }
          })
          .catch(e => console.log(e))
      }
    },
    flag: mensagem => {
      const args = mensagem.content.replace("!giga ", '').replace(' ', '')
      if (args.length > 0) {
        const data = {
          data: args
        }
        axios.post('http://127.0.0.1:5000/flag', data, {
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(res => {
          res = res.data
          console.log(res)
          const embed = new Discord.MessageEmbed()
            .setTitle(`País: ${res.nome} `)
            .addField(`Capital: `, res.capital)
            .setImage(res.bandeira)
          mensagem.channel.send(embed)
        })
      }
    }
  }