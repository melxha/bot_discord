const axios = require('axios')
module.exports = {
    bandeiras: mensagem => {
        const playing = []
        const content = mensagem.content.replace('!bandeiras ', '')
        const args = content.split(' ')
        const players = []
        args.forEach((element, index) => {
            if (index > 0 && element != ' ') {
                playing.push(element.replace('<@!', '').replace('>',''))
                const obj = {
                    player: element,
                    pontos: 0
                }
                players.push(obj)
            }
        }) 
        const data = {
            round: args[0]
        }
        axios.post('http://127.0.0.1:5000/bandeiras', data, {
          headers: {
            'Content-Type': 'application/json'
          }
        })
            .then(res => {
                res = res.data
                if (res.success) {
                    let contador = 0
                    function resultado() {
                        players.forEach(element => {
                            let str = `Jogador: ${element.player} fez ${element.pontos} pontos!`
                            mensagem.channel.send(str)
                        })
                    }

                    async function acionaJogo(c) {
                        if (c == res.paises.length) return resultado()
                        const element = res.paises[c]
                        console.log(element)
                        mensagem.channel.send(element.bandeira)
                        let acerto = false
                        let resChannel
                        try {
                            var resposta = await mensagem.channel.awaitMessages(msg => msg.content.toLowerCase() == element.nome.toLowerCase() && playing.includes(msg.author.id), {
                                max: 1,
                                time: 60000,
                                errors: ['time']
                            }).then(res => {
                                resChannel = res
                                acerto = true
                            })
                        } catch {
                            mensagem.channel.send(`Ninguém acertou! A resposta era... ${element.nome}`)
                            
                            if (c < res.paises.length) {
                                acionaJogo(c+1)
                            }
                        }
                        if (acerto) {
                            
                            const player = `<@!${resChannel.first().author.id}>`
                            players.forEach(element => {
                                if (element.player == player) element.pontos += 1
                            })
                            if (c < res.paises.length) {
                                acionaJogo(c+1)
                            }
                        }
                    }
                    acionaJogo(contador)
                }
            })
            .catch(e => console.log(e))
    }
}